import React, { Component } from "react";
import { auth } from "../firebase";
import * as routes from "../constants/routes";
import { withRouter } from "react-router-dom";
import axios from "axios";
import * as firebase from "firebase";
import MaskedInput from "react-maskedinput";
import Telefone from "../Telefone";
import ReactGA from 'react-ga';
ReactGA.initialize('UA-51170795-3');

const byPropKey = (propertyName, value) => () => ({ [propertyName]: value });

const INITIAL_STATE = {
  email: "",
  passwordOne: "",
  error: null,
  loader: false,
};

class Login extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      ...INITIAL_STATE,
    };
    this.handleSubmit = this.handleSubmit.bind(this);

    this.eventePrevent = this.eventePrevent.bind(this);
  }

  eventePrevent(event) {
    event.preventDefault();
  }

  replaceAll(str, needle, replacement) {
    return str.split(needle).join(replacement);
  }

  handleSubmit(event) {
    event.preventDefault();
    const { email, passwordOne, error } = this.state;
    const { history } = this.props;
    var html = localStorage.getItem("content");
    this.setState(byPropKey("loader", true));
    axios
      .post("/api/usuariolegado/login", {
        login: email,
        senha: passwordOne,
      })
      .then((res) => {
        if (res.data.length === 0) {
          this.setState(byPropKey("loader", false));
          localStorage.removeItem("usuario");
          this.setState(
            byPropKey("error", { message: "Usuario ou senha invalido" })
          );
        } else {
          axios
            .get("/api/breifing/usuario/" + res.data[0].id)
            .then((usuario) => {
              if (
                usuario.data.length === 0 ||
                (usuario.data[0] && usuario.data[0].role === 2)
              ) {
                localStorage.setItem("usuario", JSON.stringify(res.data[0]));
                history.push(routes.TELEFONE);
              } else {
                axios
                  .post("/api/briefinglegado", {
                    idCustomer: res.data[0].id_customer,
                    html: html.substr(
                      html.indexOf("</h2>") + 5,
                      html.length - html.indexOf("</h2>") +  5
                    ),
                    idUsuario: res.data[0].id,
                    nomeProjeto: html.substr(
                      html.indexOf("<h1>") + 4,
                      html.indexOf("</h1>") - 4
                    ),
                    excerpt: html.substr(
                      html.indexOf("<h2>") + 4 ,
                      html.indexOf("</h2>")  - html.indexOf("<h2>") - 4
                    )
                  })
                  .then((res) => {
                    this.setState(byPropKey("loader", false));
                    localStorage.setItem("idBriefing", res.data.idBreifing)
                    history.push(routes.SUCESSO);
                  })
                  .catch((error) => {
                    this.setState(byPropKey("loader", false));
                    this.setState(byPropKey("error", error));
                  });
              }
            })
            .catch((error) => {
              this.setState(byPropKey("loader", false));
              this.setState(byPropKey("error", error));
            });
        }
      })
      .catch((error) => {
        this.setState(byPropKey("loader", false));
        this.setState(byPropKey("error", error));
      });
  }

  render() {
    const { email, passwordOne, error, loader } = this.state;

    const isInvalid = email === "" || passwordOne === "";
    ReactGA.event({
      category: 'CROWD MATCH',
      action: 'login',
      label: 'Adwords',
      nonInteraction: true
    });

    return (
      <div class="card-body">
        <div class="row">
          <div class="col-sm-12">
            <p class="center lead caps heading">Identifique-se</p>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <Telefone paginaLogin="true"/>
          </div>
          <div class="col">
            <div class="jumbotron backGroundCard p-0">
              <div class="container">
                <div class="card">
                  <div class="card-header">Login</div>
                  <div class="card-body">
                    <form onSubmit={this.handleSubmit}>
                      <div class="form-group">
                        <div class="form-group">
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="basic-addon1">
                                @
                              </span>
                            </div>
                            <input
                              value={email}
                              onChange={(event) =>
                                this.setState(
                                  byPropKey("email", event.target.value)
                                )}
                              type="email"
                              class="form-control"
                              size="2"
                              placeholder="email"
                              id="Email"
                            />
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="basic-addon1">
                                *
                              </span>
                            </div>
                            <input
                              value={passwordOne}
                              onChange={(event) =>
                                this.setState(
                                  byPropKey("passwordOne", event.target.value)
                                )}
                              type="password"
                              placeholder="senha"
                              class="form-control"
                              size="2"
                              id="Senha"
                            />
                          </div>
                        </div>
                        {!loader && (
                          <button
                            disabled={isInvalid || loader}
                            type="submit"
                            class="btn btn-dark login-btn"
                            id="nextBotton"
                          >
                            Logar e prosseguir
                          </button>
                        )}
                        {loader && <div class="loader" />}
                        <div
                          ref={(ref) => (this.nextBotton = ref)}
                          class="captcha"
                        />
                      </div>
                      {error && (
                        <div class="alert alert-danger" role="alert">
                          {error.message}
                        </div>
                      )}
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
