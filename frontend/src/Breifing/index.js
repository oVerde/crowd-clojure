// @flow
import React, { Component } from "react";
// import RichText from "../rich-text";
import "../App.css";
import * as routes from "../constants/routes";
import { withRouter } from "react-router-dom";
import ForcedLayout from "../forced-layout";
import ReactGA from 'react-ga';
ReactGA.initialize('UA-51170795-3');

const byPropKey = (propertyName, value) => () => ({ [propertyName]: value });

class Breifing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      count: 0,
      disabled: true,
      hasMinimalWords: this.hasMinimalWords,
      wordCount: this.wordCount,
    };

    localStorage.clear();

    this.count = 0;
    this.enviaLogin = this.enviaLogin.bind(this);
    this.isInvalid = this.isInvalid.bind(this);
  }

  enviaLogin() {
    if (!this.state.disabled) {
      return;
    }
    const { history } = this.props;
    history.push(routes.LOGIN);
  }

  isInvalid() {
    return localStorage.getItem("content") === "<h1></h1><h2></h2><p></p>";
  }
  
  render() {
    console.log(this.state);
    const error = this.state;
    ReactGA.event({
      category: 'CROWD MATCH',
      action: 'Briefing',
      label: 'Adwords',
      nonInteraction: true
    });
    return (
      <div class="card-body">
        
        <progress
          max="60"
          value={this.state.count}
          class="btn btn-dark primeiroBotao">
        </progress>
        <button
          title="Mínimo de 60 palavras"
          type="button"
          class="btn btn-dark primeiroBotao"
          onClick={this.enviaLogin}
          disabled={!this.state.disabled}
        >
          &#x25BA; {this.state.disabled || 31 - this.state.count + " |"} CROWD MATCH &#x25C4;
        </button>
        <ForcedLayout
          parent={(c) => { this.setState({ count: c }) }}
          disable={(d) => { this.setState({ disabled: d }) }}
        />
      </div>
    );    
  }
}

export default withRouter(Breifing);
