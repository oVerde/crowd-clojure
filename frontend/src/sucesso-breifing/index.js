import React, { Component } from "react";
import "../App.css";
import $ from "jquery";
import axios from "axios";
import ReactGA from 'react-ga';
ReactGA.initialize('UA-51170795-3');


/** SNIPPET GUARDAR
//    axios
//    .post(
//      "/api/match",
//      {
//        idBreifing: localStorage.getItem("idBriefing"),
//      },
//      {
//        "Access-Control-Allow-Origin": "*",
//      }
//    )
//    .then((usuario) => {});
**/

class SucessoBreifing extends Component {
  componentDidMount() {
    (usuario) => {}
    $(document).ready(function() {
      // Progress
      var progressBar = $(".loading-bar span");
      var progressAmount = $(".loading-bar").attr("data-progress");
      progressAmount = 0;

      var loadingDelay = setTimeout(function() {
        var interval = setInterval(function() {
          progressAmount += 7;

          progressBar.css("width", progressAmount + "%");

          if (progressAmount >= 100) {
            setTimeout(function() {
              clearInterval(interval);
              reverseAnimation();
              $('body').addClass('pointer').on('click', function () {
                document.location.replace('https://CROWD.br.com');
              })
            }, 300);
          }
        }, 300);

      }, 2000);


      // Processing over
      function reverseAnimation() {
        $("#processing").removeClass("uncomplete").addClass("complete");
        $("canvas").fadeOut();
        $("#h1").text("Tudo pronto!");
        $("#h2").text("Você receberá respostas em seu email ainda hoje.");
      }

      var canvas = document.querySelector("canvas"),
        ctx = canvas.getContext("2d"),
        particles = [],
        patriclesNum = 100,
        w = 500,
        h = 500,
        colors = [
          "#333",
          "#84c9f2",
        ];

      canvas.width = 500;
      canvas.height = 500;

      function Factory() {
        this.x = Math.round(Math.random() * w);
        this.y = Math.round(Math.random() * h);
        this.rad = Math.round(Math.random() * 1) + 1;
        this.rgba = colors[Math.round(Math.random() * 3)];
        this.vx = Math.round(Math.random() * 3) - 1.5;
        this.vy = Math.round(Math.random() * 3) - 1.5;
      }

      function draw() {
        ctx.clearRect(0, 0, w, h);
        for (var i = 0; i < patriclesNum; i++) {
          var temp = particles[i];
          var factor = 1;

          for (var j = 0; j < patriclesNum; j++) {
            var temp2 = particles[j];
            ctx.linewidth = 1;

            if (temp.rgba == temp2.rgba && findDistance(temp, temp2) < 50) {
              ctx.strokeStyle = temp.rgba;
              ctx.beginPath();
              ctx.moveTo(temp.x, temp.y);
              ctx.lineTo(temp2.x, temp2.y);
              ctx.stroke();
              factor++;
            }
          }

          ctx.fillStyle = temp.rgba;
          ctx.strokeStyle = temp.rgba;

          ctx.beginPath();
          ctx.arc(temp.x, temp.y, temp.rad * factor, 0, Math.PI * 2, true);
          ctx.fill();
          ctx.closePath();

          ctx.beginPath();
          ctx.stroke();
          ctx.closePath();

          temp.x += temp.vx;
          temp.y += temp.vy;

          if (temp.x > w) temp.x = 0;
          if (temp.x < 0) temp.x = w;
          if (temp.y > h) temp.y = 0;
          if (temp.y < 0) temp.y = h;
        }
      }

      function findDistance(p1, p2) {
        return Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
      }

      let requestAnimFrame = (function() {
        return (
          window.requestAnimationFrame ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame ||
          function(callback) {
            window.setTimeout(callback, 1000 / 60);
          }
        );
      })();

      (function init() {
        for (var i = 0; i < patriclesNum; i++) {
          particles.push(new Factory());
        }
      })();

      (function loop() {
        draw();
        requestAnimFrame(loop);
      })();
    });
  }

  render() {
  ReactGA.event({
    category: 'CROWD MATCH',
    action: 'Publicado',
    label: 'Adwords',
    nonInteraction: true
  });
    return (
      <div id="processing" className="uncomplete center">
        <div className="headings">
          <h1 id="h1">CROWD MATCH processando o seu pedido</h1>
          <h2 id="h2">Só mais um momento</h2>
        </div>
        <div className="wrapper">
          <div className="gears">
            <canvas />
          </div>
          <div className="loading-bar" data-progress="0">
            <span />
          </div>
          <svg
            className="checkmark checkmark-success"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 50 50"
          >
            <circle
              className="checkmark-circle"
              cx="25"
              cy="25"
              r="25"
              fill="none"
            />
            <path
              className="checkmark-check"
              fill="none"
              d="M14.1 27.2l7.1 7.2 16.7-16.8"
            />
          </svg>
          {/* <svg class="checkmark checkmark-error" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><circle class="checkmark-circle" cx="25" cy="25" r="25" fill="none" /><path class="checkmark-check" fill="none" d="M16,34L25 25 34 16 " /><path class="checkmark-check" fill="none" d="M16,16L25 25 34 34 " /></svg> */}
        </div>
      </div>
    );
  }
}

export default SucessoBreifing;
