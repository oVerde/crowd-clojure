import { Editor } from "slate-react";
import { Block, Value } from "slate";
import { CHILD_REQUIRED, CHILD_TYPE_INVALID } from "slate-schema-violations";

import React from "react";
import initialValue from "./value.json";
import empty from "./empty.json";
import { isKeyHotkey } from "is-hotkey";
import Html from "slate-html-serializer";

/**
 * Define the default node type.
 *
 * @type {String}
 */

const BLOCK_TAGS = {
  blockquote: "quote",
  ul: "bullet-list",
  ol: "numbered-list",
  p: "paragraph",
  pre: "code",
  h1: "heading-one",
  h2: "heading-two",
};

const MARK_TAGS = {
  em: "italic",
  strong: "bold",
  u: "underlined",
};
const rules = [
  {
    deserialize(el, next) {
      const type = BLOCK_TAGS[el.tagName.toLowerCase()];
      if (type) {
        return {
          object: "block",
          type: type,
          nodes: next(el.childNodes),
        };
      }
    },
    serialize(obj, children) {
      if (obj.object == "block") {
        switch (obj.type) {
          case "code":
            return (
              <pre>
                <code>{children}</code>
              </pre>
            );
          case "paragraph":
            return <p>{children}</p>;
          case "heading-one":
            return <h1>{children}</h1>;
          case "heading-two":
            return <h2>{children}</h2>;
          case "block-quote":
            return <blockquote>{children}</blockquote>;
          case "bulleted-list":
            return <ul>{children}</ul>;
          case "list-item":
            return <li>{children}</li>;
          case "numbered-list":
            return <ol>{children}</ol>;
        }
      }
    },
  },
  // Add a new rule that handles marks...
  {
    deserialize(el, next) {
      const type = MARK_TAGS[el.tagName.toLowerCase()];
      if (type) {
        return {
          object: "mark",
          type: type,
          nodes: next(el.childNodes),
        };
      }
    },
    serialize(obj, children) {
      if (obj.object == "mark") {
        switch (obj.type) {
          case "bold":
            return <strong>{children}</strong>;
          case "italic":
            return <em>{children}</em>;
          case "underlined":
            return <u>{children}</u>;
          case "code":
            return <u>{children}</u>;
        }
      }
    },
  },
];

const DEFAULT_NODE = "paragraph";

/**
 * Define hotkey matchers.
 *
 * @type {Function}
 */

const isBoldHotkey = isKeyHotkey("mod+b");
const isItalicHotkey = isKeyHotkey("mod+i");
const isUnderlinedHotkey = isKeyHotkey("mod+u");
const isCodeHotkey = isKeyHotkey("mod+`");
const html = new Html({ rules });

/**
 * A simple schema to enforce the nodes in the Slate document.
 *
 * @type {Object}
 */

const schema = {
  document: {
    nodes: [
      {
        types: [
          "heading-one",
        ],
        min: 1,
        max: 1,
      },
      {
        types: [
          "heading-two",
        ],
        min: 1,
        max: 1,
      },
      {
        types: [
          "paragraph",
          "block-quote",
          "bulleted-list",
          "list-item",
          "numbered-list",
        ],
        min: 1,
      },
    ],
    normalize: (change, violation, { node, child, index }) => {
      switch (violation) {
        case CHILD_TYPE_INVALID: {
          return change.setNodeByKey(
            child.key,

            index == 0 ? "heading-one" :
              index == 1 ? "heading-two" :
                "paragraph"
          );
        }
        case CHILD_REQUIRED: {
          const block = Block.create(

            index == 0 ? "heading-one" :
              index == 1 ? "heading-two" :
                "paragraph"
          );
          return change.insertNodeByKey(node.key, index, block);
        }
      }
    },
  },
};

/**
 * The Forced Layout example.
 *
 * @type {Component}
 */

class ForcedLayout extends React.Component {
  /**
   * Deserialize the initial editor value.
   *
   * @type {Object}
   */

  state = {
    value: Value.fromJSON(initialValue),
    placeholder: true
  };

  /**
   * Get the block type for a series of auto-markdown shortcut `chars`.
   *
   * @param {String} chars
   * @return {String} block
   */

  getType = (chars) => {
    switch (chars) {
      case "*":
      case "-":
      case "+":
        return "list-item";
      case "1.":
        return "numbered-list";
      case ">":
        return "block-quote";
      case "#":
        return "heading-one";
      case "##":
        return "heading-two";
      case "###":
        return "heading-three";
      case "####":
        return "heading-four";
      case "#####":
        return "heading-five";
      case "######":
        return "heading-six";
      default:
        return null;
    }
  };

  /**
   * Check if the current selection has a mark with `type` in it.
   *
   * @param {String} type
   * @return {Boolean}
   */

  hasMark = (type) => {
    const { value } = this.state;
    return value.activeMarks.some((mark) => mark.type === type);
  };

  /**
   * Check if the any of the currently selected blocks are of `type`.
   *
   * @param {String} type
   * @return {Boolean}
   */

  hasBlock = (type) => {
    const { value } = this.state;
    return value.blocks.some((node) => node.type === type);
  };

  renderPlaceholder(props) {
    const { node, editor } = props
    if (node.object != 'block') return
    if (node.type != 'heading-one') return
    if (node.text != '') return

    return (
      <div>
        <h1 class="linha"><em>Aqui o título do seu briefing.</em></h1><h2 class="linha"><em>Escreva seu objetivo no próximo paragrafo.</em></h2>
        <p class="linha"><em>Agora descreva bem o job, especifique o que o profissional precisará te entregar. Quanto mais completo melhor! Caso haja deadline e se o trabalho exige que o profissional compareça é importante especificar também.<br />
          <br />
          Para projetos confidenciais, recomendo não mencionar para quem é o cliente nessa fase.<br />
          <br />
          Não sugerimos inserir budget, pois você receberá mais de uma proposta.<br />
          <br />
          Boa escrita!</em></p>
      </div>
    )
  };

  /**
   * On change, save the new `value`.
   *
   * @param {Change} change
   */

  onChange = ({ value }) => {
    let anterior = localStorage.getItem("content");
    localStorage.removeItem("content");
    const string = html.serialize(value);
    localStorage.setItem("content", string);
    this.setState({ value });
    this.props.parent(localStorage.getItem('content') ?
      localStorage.getItem('content')
      .split(' ')
      .length :
      false);
    this.props.disable(localStorage.getItem('content') ?
      localStorage.getItem('content')
        .split(' ')
        .length >= 31
      : false);
  };

  onClick = ({ value }) => {
    const string = localStorage.getItem("content")
    if (string.indexOf('<h1></h1><h2></h2><p></p>') !== -1) {
      var value = Value.fromJSON(empty)
      this.setState({ value })
      setTimeout(() => {
        this.elem.focus();
      })
    }
  }
  /**
   * On space, if it was after an auto-markdown shortcut, convert the current
   * node into the shortcut's corresponding type.
   *
   * @param {Event} event
   * @param {Change} change
   */

  onSpace = (event, change) => {
    const { value } = change;
    if (value.isExpanded) return;

    const { startBlock, startOffset } = value;
    const chars = startBlock.text.slice(0, startOffset).replace(/\s*/g, "");
    const type = this.getType(chars);

    if (!type) return;
    if (type == "list-item" && startBlock.type == "list-item") return;
    event.preventDefault();

    change.setBlocks(type);

    if (type == "list-item") {
      change.wrapBlock("bulleted-list");
    }

    change.extendToStartOf(startBlock).delete();
    return true;
  };

  /**
   * On key down, if it's a formatting command toggle a mark.
   *
   * @param {Event} event
   * @param {Change} change
   * @return {Change}
   */

  onKeyDown = (event, change) => {

    let mark;

    switch (event.key) {
      case " ":
        return this.onSpace(event, change);
    }

    if (isBoldHotkey(event)) {
      mark = "bold";
    } else if (isItalicHotkey(event)) {
      mark = "italic";
    } else if (isUnderlinedHotkey(event)) {
      mark = "underlined";
    } else if (isCodeHotkey(event)) {
      mark = "code";
    } else {
      return;
    }

    event.preventDefault();
    change.toggleMark(mark);
    return true;
  };

  /**
   * When a mark button is clicked, toggle the current mark.
   *
   * @param {Event} event
   * @param {String} type
   */

  onClickMark = (event, type) => {
    event.preventDefault();
    const { value } = this.state;
    const change = value.change().toggleMark(type);
    this.onChange(change);
  };

  /**
   * When a block button is clicked, toggle the block type.
   *
   * @param {Event} event
   * @param {String} type
   */

  onClickBlock = (event, type) => {
    event.preventDefault();
    const { value } = this.state;
    const change = value.change();
    const { document } = value;

    // Handle everything but list buttons.
    if (type !== "bulleted-list" && type !== "numbered-list") {
      const isActive = this.hasBlock(type);
      const isList = this.hasBlock("list-item");

      if (isList) {
        change
          .setBlocks(

            isActive ? DEFAULT_NODE :
              type
          )
          .unwrapBlock("bulleted-list")
          .unwrapBlock("numbered-list");
      } else {
        change.setBlocks(

          isActive ? DEFAULT_NODE :
            type
        );
      }
    } else {
      // Handle the extra wrapping required for list buttons.
      const isList = this.hasBlock("list-item");
      const isType = value.blocks.some((block) => {
        return !!document.getClosest(
          block.key,
          (parent) => parent.type === type
        );
      });

      if (isList && isType) {
        change
          .setBlocks(DEFAULT_NODE)
          .unwrapBlock("bulleted-list")
          .unwrapBlock("numbered-list");
      } else if (isList) {
        change
          .unwrapBlock(

            type === "bulleted-list" ? "numbered-list" :
              "bulleted-list"
          )
          .wrapBlock(type);
      } else {
        change.setBlocks("list-item").wrapBlock(type);
      }
    }

    this.onChange(change);
  };

  /**
   * Render the editor.
   *
   * @return {Component} component
   */

  render() {
    
    return (
      <div className="editor">
        {this.renderToolbar()}
        <Editor
          className="editor-body"
          value={this.state.value}
          schema={schema}
          renderPlaceholder={this.renderPlaceholder.bind(this)}
          renderNode={this.renderNode}
          onChange={this.onChange}
          onClick={this.onClick}
          onKeyDown={this.onKeyDown}
          renderMark={this.renderMark}
          plugins={this.state.WordCount}
          spellCheck
          autoFocus
          ref={(elem) => {this.elem = elem}}
        />
      </div>
    );
  }

  /**
   * Render the toolbar.
   *
   * @return {Element}
   */

  renderToolbar = () => {
    return (
      <div className="menu toolbar-menu">
        {this.renderMarkButton("bold", "format_bold")}
        {this.renderMarkButton("italic", "format_italic")}
        {this.renderMarkButton("underlined", "format_underlined")}
        {this.renderMarkButton("code", "code")}
        {/* {this.renderBlockButton("heading-one", "looks_one")} */}
        {/* {this.renderBlockButton("heading-two", "looks_two")} */}
        {this.renderBlockButton("block-quote", "format_quote")}
        {this.renderBlockButton("numbered-list", "format_list_numbered")}
        {this.renderBlockButton("bulleted-list", "format_list_bulleted")}
      </div>
    );
  };

  /**
   * Render a mark-toggling toolbar button.
   *
   * @param {String} type
   * @param {String} icon
   * @return {Element}
   */

  renderMarkButton = (type, icon) => {
    const isActive = this.hasMark(type);
    const onMouseDown = (event) => this.onClickMark(event, type);

    return (
      // eslint-disable-next-line react/jsx-no-bind
      <span className="button" onMouseDown={onMouseDown} data-active={isActive}>
        <span className="material-icons">{icon}</span>
      </span>
    );
  };

  /**
   * Render a block-toggling toolbar button.
   *
   * @param {String} type
   * @param {String} icon
   * @return {Element}
   */

  renderBlockButton = (type, icon) => {
    const isActive = this.hasBlock(type);
    const onMouseDown = (event) => this.onClickBlock(event, type);

    return (
      // eslint-disable-next-line react/jsx-no-bind
      <span className="button" onMouseDown={onMouseDown} data-active={isActive}>
        <span className="material-icons">{icon}</span>
      </span>
    );
  };

  /**
   * Render a Slate node.
   *
   * @param {Object} props
   * @return {Element}
   */

  renderNode = (props) => {
    const { attributes, children, node } = props;
    switch (node.type) {
      case "block-quote":
        return <blockquote {...attributes}>{children}</blockquote>;
      case "bulleted-list":
        return <ul {...attributes}>{children}</ul>;
      case "heading-one":
        return <h1 {...attributes}>{children}</h1>;
      case "heading-two":
        return <h2 {...attributes}>{children}</h2>;
      case "heading-three":
        return <h3 {...attributes}>{children}</h3>;
      case "heading-four":
        return <h4 {...attributes}>{children}</h4>;
      case "heading-five":
        return <h5 {...attributes}>{children}</h5>;
      case "heading-six":
        return <h6 {...attributes}>{children}</h6>;
      case "list-item":
        return <li {...attributes}>{children}</li>;
      case "numbered-list":
        return <ol {...attributes}>{children}</ol>;
    }
  };

  /**
   * Render a Slate mark.
   *
   * @param {Object} props
   * @return {Element}
   */

  renderMark = (props) => {
    const { children, mark } = props;
    switch (mark.type) {
      case "bold":
        return <strong>{children}</strong>;
      case "code":
        return <code>{children}</code>;
      case "italic":
        return <em>{children}</em>;
      case "underlined":
        return <u>{children}</u>;
      default:
        return;
    }
  };
}

/**
 * Export.
 */

export default ForcedLayout;
