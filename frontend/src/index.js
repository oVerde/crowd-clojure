// @flow
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import autofill from 'react-autofill';

ReactDOM.render(React.createElement(autofill(App)), document.getElementById("root"));
registerServiceWorker();
