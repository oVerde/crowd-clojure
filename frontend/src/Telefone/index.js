import React, { Component } from "react";
import { auth } from "../firebase";
import * as routes from "../constants/routes";
import { withRouter } from "react-router-dom";
import axios from "axios";
import * as firebase from "firebase";
import MaskedInput from "react-maskedinput";
import ReactGA from 'react-ga';
ReactGA.initialize('UA-51170795-3');

const byPropKey = (propertyName, value) => () => ({ [propertyName]: value });

const INITIAL_STATE = {
  telefone: "",
  recaptcha: null,
  error: null,
  usuario: JSON.parse(localStorage.getItem("usuario")),
};

class Telefone extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      ...INITIAL_STATE,
    };
    console.log(this.state);
    var { usuario } = this.state;
    this.handleSubmit = this.handleSubmit.bind(this);

    this.eventePrevent = this.eventePrevent.bind(this);
  }

  componentDidMount() {
    var { recaptcha } = this.state;
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
      this.nextBotton,
      {
        size: "invisible",
        callback: function(response) {},
      }
    );
  }

  eventePrevent(event) {
    event.preventDefault();
  }

  replaceAll(str, needle, replacement) {
    return str.split(needle).join(replacement);
  }

  handleSubmit(event) {
    event.preventDefault();
    const { telefone, recaptcha, error } = this.state;
    const { history } = this.props;
    this.setState(byPropKey('loader', true));
    var telefoneSemFormatacao = this.replaceAll(
      this.replaceAll(this.replaceAll(telefone, "(", ""), ")", ""),
      " ",
      ""
    );
    localStorage.setItem("telefone", telefoneSemFormatacao);
    auth
      .enviarCodigoAoCelular(telefoneSemFormatacao, window.recaptchaVerifier)
      .then((confirmationResult) => {
        window.confirmationResult = confirmationResult;
        history.push(routes.CONFIRMACAO);
      })
      .catch((error) => {
        this.setState(byPropKey("error", error));
      });
  }

  render() {
    const { telefone, error, usuario, loader} = this.state;

    const isInvalid = telefone === "";

    ReactGA.event({
      category: 'CROWD MATCH',
      action: 'Telefone',
      label: 'Adwords',
      nonInteraction: true
    });

    return (
      <div class={"jumbotron backGroundCard " + (this.props.paginaLogin ? 'p-0' : '')}>
        {usuario &&
        usuario.role === 2 && !this.props.paginaLogin && (
          <p class="lead">
            Encontrei que você é um freelancer, vamos criar uma conta nova!
          </p>
        )}
        <div class="container">
          <div class="card">
          {
            this.props.paginaLogin ? 
                (<div class="card-header">Não possui cadastro? Cadastre-se agora.</div>)
              : (<div class="card-header">Conclua com seu telefone</div>)  
          }
            <div class="card-body">
              <form onSubmit={this.handleSubmit}>
                <div class="form-group">
                  <div class="form-group">
                    <label for="telefone">Telefone</label>
                    <MaskedInput
                      value={telefone}
                      mask="+55 (11) 11111-1111"
                      onChange={(event) =>
                        this.setState(
                          byPropKey("telefone", event.target.value)
                        )}
                      class="form-control"
                      id="Telefone"
                    />
                  </div>
                  <div class="form-group">
                    {!loader && (
                      <button
                      type="submit"
                      class="btn btn-dark login-btn"
                      id="nextBotton"
                    >
                      Registrar
                    </button>
                    )}
                    {loader && <div class="loader" />}
                  </div>
                  <div className="row">
                    <div className="col">
                      <div
                        ref={(ref) => (this.nextBotton = ref)}
                        class="captcha"
                        className="center"
                      />
                    </div>
                  </div>
                </div>
                {error && (
                  <div class="alert alert-danger" role="alert">
                    {error.message}
                  </div>
                )}
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Telefone);
