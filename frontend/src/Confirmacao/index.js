import React, {Component} from 'react';
import {auth} from '../firebase';
import * as routes from '../constants/routes';
import {withRouter} from 'react-router-dom';
import axios from 'axios';
import MaskedInput from 'react-maskedinput';
import ReactGA from 'react-ga';
ReactGA.initialize('UA-51170795-3');

const byPropKey = (propertyName, value) => () => ({[propertyName]: value});

const INITIAL_STATE = {
    codigo: '',
    error: null,
    loader: false
};

class Confirmacao extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ...INITIAL_STATE
        };

        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        const {codigo, error} = this.state;
        var {loader} = this.state;
        const {history} = this.props;
        var html = localStorage.getItem('content')
        var usuarioLogado = JSON.parse(localStorage.getItem('usuario'))
        console.log(usuarioLogado)
        this.setState(byPropKey('loader', true));
        window.confirmationResult
            .confirm(codigo)
            .then(result => {
                axios.get('/api/breifing/' + result.user.uid)
                    .then(res => {
                        if(res.data.length == 0 && (usuarioLogado === null 
                            || (usuarioLogado && usuarioLogado.role === 2))) {
                            localStorage.setItem('uid', result.user.uid),
                            history.push(routes.CRIAR_USUARIO);
                        } else {
                            if (res.data.length === 0) {
                                axios.post('/api/breifing', {
                                    idUsuarioFire: localStorage.getItem('uid'),
                                    html: localStorage.getItem('content'),
                                    idCostumer : usuarioLogado.id_customer,
                                    idUsuario : usuarioLogado.id
                                }).then(res => {
                                    this.setState(byPropKey('loader', false));
                                    this.criarBriefing(usuarioLogado.id, usuarioLogado.id_customer)
                                }).catch(error => {
                                    this.setState(byPropKey('loader', false));
                                    this.setState(byPropKey('error', error));
                                });
                            } else {
                                usuarioLogado = res.data[0]
                                this.setState(byPropKey('loader', false));
                                this.criarBriefing(usuarioLogado.idUsuario, usuarioLogado.idCostumer)
                            }                            
                        }
                    })
            })
            .catch(error => {
                this.setState(byPropKey('loader', false));
                this.setState(byPropKey('error', error));
            });
    }

    criarBriefing(id, idCustomer) {
        var html = localStorage.getItem('content')
        const {history} = this.props;
        const {error} = this.state;
        axios.post('/api/briefinglegado', {
            idCustomer : idCustomer,
            html: html.substr(
                html.indexOf("</h2>") + 5,
                html.length - html.indexOf("</h2>") +  5
              ),
            idUsuario : id,
            nomeProjeto: html.substr(
                html.indexOf("<h1>") + 4,
                html.indexOf("</h1>") - 4
              ),
              excerpt: html.substr(
                html.indexOf("<h2>") + 4 ,
                html.indexOf("</h2>")  - html.indexOf("<h2>") - 4
              )
        }).then(res => {
            localStorage.setItem("idBriefing", res.data.idBreifing)   
            history.push(routes.SUCESSO);
        }).catch(error => {
            this.setState(byPropKey('error', error));
        });
    }

    render() {
        const {codigo, error, loader} = this.state;

        const isInvalid = codigo === '' ;

        ReactGA.event({
          category: 'CROWD MATCH',
          action: 'Token',
          label: 'Adwords',
          nonInteraction: true
        });

        return (
            <div class="jumbotron backGroundCard">
                <div class="container">
                    <div class="card">
                        <div class="card-header">
                            Confirmação do telefone
                        </div>
                        <div class="card-body">
                            <form onSubmit={this.handleSubmit}>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="codigo">Codigo de confirmação</label>
                                        <MaskedInput
                                            value={codigo}
                                            onChange={event => this.setState(byPropKey('codigo', event.target.value))}
                                            mask="111111" 
                                            class="form-control"
                                            id="Codigo"/>
                                    </div>
                                    {!loader && <button disabled={isInvalid || loader} type="submit" class="btn btn-dark centroBotao login-btn" id="next-botton">Prosseguir</button>}
                                    {loader && <div class="loader"></div>}
                                </div>
                                {error && <div class="alert alert-danger" role="alert">{error.message}</div>}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Confirmacao);