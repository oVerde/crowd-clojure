import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyDQzewirl2Xu_N6IjCZQo9SrGMPLBd4SMY",
    authDomain: "crowd-167215.firebaseapp.com",
    databaseURL: "https://crowd-167215.firebaseio.com",
    projectId: "crowd-167215",
    storageBucket: "crowd-167215.appspot.com",
    messagingSenderId: "1026241189971"
  };

  if (!firebase.apps.length) {
    firebase.initializeApp(config);
  }
  
  firebase.auth().languageCode = 'pt-br';
  const auth = firebase.auth();
  
  export {
    auth,
  };