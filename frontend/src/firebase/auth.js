import { auth } from './firebase';

export const criarUsuarioComEmail = (email, password) =>
  auth.createUserWithEmailAndPassword(email, password);

export const LogarComEmail = (email, password) =>
  auth.signInWithEmailAndPassword(email, password);

export const logout = () =>
  auth.signOut();

export const reseteSenha = (email) =>
  auth.sendPasswordResetEmail(email);

export const enviarCodigoAoCelular = (celular, recaptcha) =>
  auth.signInWithPhoneNumber(celular, recaptcha);

