import React, { Component } from "react";
import { auth } from "../firebase";
import * as routes from "../constants/routes";
import { withRouter } from "react-router-dom";
import axios from "axios";
import MaskedInput from "react-maskedinput";
import ReactGA from 'react-ga';
ReactGA.initialize('UA-51170795-3');

const byPropKey = (propertyName, value) => () => ({ [propertyName]: value });

const INITIAL_STATE = {
  username: "",
  cnpj: "",
  email: "",
  passwordOne: "",
  passwordTwo: "",
  error: null,
  loader: false,
  pattern: "111.111.111-11"
};

class Cadastro extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...INITIAL_STATE,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    const { email, cnpj, passwordOne, username } = this.state;
    const { history } = this.props;
    var { loader } = this.state;
    var html = localStorage.getItem("content");
    this.setState(byPropKey("loader", true));
    axios
      .get("/api/usuariolegado?email=" + email)
      .then((existente) => {
        if (existente.data.length == 0) {
          axios
            .post(
              "/api/usuariolegado",
              {
                nome: username,
                cnpj: cnpj,
                email: email,
                senha: passwordOne,
                phone: localStorage.getItem("telefone")
              },
              {
                "Access-Control-Allow-Origin": "*",
              }
            )
            .then((usuario) => {
              axios
                .post("/api/breifing", {
                  idUsuarioFire: localStorage.getItem("uid"),
                  html: localStorage.getItem("content"),
                  idCostumer: usuario.data.idCustomer,
                  idUsuario: usuario.data.idUsuario,
                })
                .then((res) => {
                  axios
                    .post("/api/briefinglegado", {
                      idCustomer: usuario.data.idCustomer,
                      html: html.substr(
                        html.indexOf("</h2>") + 5,
                        html.length - html.indexOf("</h2>") +  5
                      ),
                      idUsuario: usuario.data.idUsuario,
                      nomeProjeto: html.substr(
                        html.indexOf("<h1>") + 4,
                        html.indexOf("</h1>") - 4
                      ),
                      excerpt: html.substr(
                        html.indexOf("<h2>") + 4 ,
                        html.indexOf("</h2>")  - html.indexOf("<h2>") - 4
                      )
                    })
                    .then((res) => {
                      this.setState(byPropKey("loader", false));
                      localStorage.setItem("idBriefing", res.data.idBreifing)
                      history.push(routes.SUCESSO);
                    })
                    .catch((error) => {
                      this.setState(byPropKey("loader", false));
                      this.setState(byPropKey("error", error));
                    });
                })
                .catch((error) => {
                  this.setState(byPropKey("loader", false));
                  this.setState(byPropKey("error", error));
                });
            });
        } else {
          this.setState(byPropKey("loader", false));
          this.setState(byPropKey("error", {message : "Usuario já existente"}));

        }
      })
      .catch((error) => {
        this.setState(byPropKey("loader", false));
        this.setState(byPropKey("error", error));
      });
  }

  render() {
    const {
      username,
      cnpj,
      email,
      passwordOne,
      passwordTwo,
      error,
      loader,
    } = this.state;

    const isInvalid =
      passwordOne === "" || email === "" || cnpj === "" || username === "";

  ReactGA.event({
    category: 'CROWD MATCH',
    action: 'Cadastro',
    label: 'Adwords',
    nonInteraction: true
  });
    
    return (
      <div class="jumbotron backGroundCard p-0">
        <div class="container">
          <div style={{ marginTop: 100 }} class="card">
            <div class="card-header">Cadastro</div>
            <div class="card-body">
              <form onSubmit={this.handleSubmit}>
                <div class="form-group">
                  <div class="form-group">
                    <label for="Nome">Nome</label>
                    <input
                      value={username}
                      onChange={(event) =>
                        this.setState(
                          byPropKey("username", event.target.value)
                        )}
                      type="text"
                      class="form-control"
                      id="Nome"
                    />
                  </div>
                  <div class="form-group">
                    <label for="CNPJ">CPF/CNPJ</label>
                    <MaskedInput
                      value={cnpj}
                      mask={this.state.pattern}
                      onChange={(event) => {
                        this.setState(byPropKey("cnpj", event.target.value.replace(/\D/g, '')));
                        if (this.state.cnpj.length < 11) this.setState({pattern: '111.111.111-11*'});
                        else this.setState({ pattern: '11.111.111/1111-11' });
                      }}
                    class="form-control"
                    id="CNPJ"
                    size="11"
                  />
                  </div>
                  <div class="form-group">
                    <label for="Email">Email</label>
                    <input
                      value={email}
                      onChange={(event) =>                         
                        this.setState(byPropKey("email", event.target.value))}
                      type="email"
                      class="form-control"
                      id="Email"
                    />
                  </div>
                  <div class="form-group">
                    <label for="Senha">Senha</label>
                    <input
                      value={passwordOne}
                      onChange={(event) =>
                        this.setState(
                          byPropKey("passwordOne", event.target.value)
                        )}
                      type="password"
                      class="form-control"
                      id="Senha"
                    />
                  </div>
                  {loader && <div class="loader" />}
                  {!loader && (
                    <button
                      disabled={isInvalid || loader}
                      type="submit"
                      class="btn btn-dark centroBotao login-btn"
                    >
                      Cadastrar e publicar!
                    </button>
                  )}
                </div>
                {error && (
                  <div class="alert alert-danger" role="alert">
                    {error.message}
                  </div>
                )}
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Cadastro);
