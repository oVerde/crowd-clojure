// @flow
import React, { Component } from "react";
import Breifing from "./Breifing";
import Cadastro from "./Cadastro";
import Telefone from "./Telefone";
import Login from "./Login";
import Confirmacao from "./Confirmacao";
import SucessoBreifing from "./sucesso-breifing";
import * as routes from "./constants/routes";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./App.css";

import ReactGA from 'react-ga';
ReactGA.initialize('UA-51170795-3');
ReactGA.pageview(window.location.pathname + window.location.search);

class App extends Component {
  render() {
    return (
        <Router>
          <div>
            <a href="https://crowd.br.com">
              <img src="logo.png" srcset="logo-mini.png 208w" sizes="(max-width: 760px) 20vw, 100vw" alt="CROWD" class="logo" />
            </a>
            <div class="principal">
              <Route
                exact
                path={routes.CRIAR_USUARIO}
                component={() => <Cadastro />}
              />
              <Route
                exact
                path={routes.BREIFING}
                component={() => <Breifing />}
              />
              <Route
                exact
                path={routes.SUCESSO}
                component={() => <SucessoBreifing />}
              />
              <Route exact path={routes.LOGIN} component={() => <Login />} />
              <Route
                exact
                path={routes.TELEFONE}
                component={() => <Telefone />}
              />
              <Route
                exact
                path={routes.CONFIRMACAO}
                component={() => <Confirmacao />}
              />
            </div>
          </div>
        </Router>
    );
  }
}

export default App;
