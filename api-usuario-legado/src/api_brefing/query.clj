(ns api-brefing.query
    (:require [clojure.java.jdbc :as j]
      [clj-time.core :as t]
      [clj-time.coerce :as coerce]
      [clj-time.local :as l])
    (:import [java.security MessageDigest]))

(def db-spec {:dbtype   (if (= (System/getenv "ambiente") "prod")
                          (System/getenv "dbtype-prd")
                          (System/getenv "dbtype-hm"))
              :dbname   (if (= (System/getenv "ambiente") "prod")
                          (System/getenv "dbname-prd")
                          (System/getenv "dbname-hm"))
              :server   (if (= (System/getenv "ambiente") "prod")
                          (System/getenv "server-prd")
                          (System/getenv "server-hm"))
              :user     (if (= (System/getenv "ambiente") "prod")
                          (System/getenv "user-prd")
                          (System/getenv "user-hm"))
              :password (if (= (System/getenv "ambiente") "prod")
                          (System/getenv "password-prd")
                          (System/getenv "password-hm"))})

(defn sql-now []
      "Retorna a data atual em sql data"
      (coerce/to-sql-time (l/local-now)))

(defn md5 [s]
      (->> (-> (MessageDigest/getInstance "md5")
               (.digest (.getBytes s "UTF-8")))
           (map #(format "%02x" %))
           (apply str)))

(defn procura-login-existente [email]
      (j/query db-spec ["SELECT id, id_customer FROM crowd_users WHERE email = ?" email]))

(defn efetua-login [email senha]
      (j/query db-spec ["SELECT u.id,
                 (select c.id from crowd_customers c where u.id_customer = c.id) id_customer
                 , u.role FROM crowd_users u
                WHERE u.email = ?
                and u.password = ?" email (md5 senha)]))

(defn insert-custumer [nome cnpj email phone]
    (get (first (j/insert! db-spec :crowd_customers
                 {:name nome
                  :active 1
                  :created_at (sql-now)
                  :prospect 0
                  :cnpj cnpj
                  :qty_persons 1
                  :plan_type 1
                  :trade nome
                  :email_responsible email
                  :phone phone})) :generated_keys))

(defn insert-user [nome email senha idCustomer phone uid]
    (get (first (j/insert! db-spec :crowd_users
                 { :id_customer idCustomer
                    :name nome
                    :email email
                    :confirm_email uid
                    :role 4
                    :phone phone
                    :accept_terms 1
                    :password (md5 senha)
                    :created_at (sql-now)
                    :accept_terms_at (sql-now)}))

     :generated_keys))

(defn insert-usuario [nome cnpj email senha phone uid]
    (def idCustomer (insert-custumer nome cnpj email phone))
    { :idUsuario (insert-user nome email senha idCustomer phone uid)
      :idCustomer idCustomer})
        