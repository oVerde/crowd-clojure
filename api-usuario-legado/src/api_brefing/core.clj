(ns api-brefing.core
    (:require [compojure.core :refer :all]
      [compojure.handler :as handler]
      [compojure.route :as route]
      [ring.middleware.json :as json]
      [ring.util.response :refer [response, created]]
      [api-brefing.query :refer :all]
      [clojure.tools.logging :as log]))

(defroutes app-routes
           (POST "/api/usuariolegado" {:keys [params]}
                 (let [{:keys [nome cnpj email senha phone uid]} params]
                      (log/info "Criando user " nome cnpj email senha phone uid)
                      (response (insert-usuario nome cnpj email senha phone))))
           (POST "/api/usuariolegado/login" {:keys [params]}
                 (let [{:keys [login senha]} params]
                      (log/info "tentativa de login email " login)
                      (response (efetua-login login senha))))
           (GET "/api/usuariolegado" [email]
                (response (procura-login-existente email)))
           (route/resources "/")
           (route/not-found "Not Found"))

(def app
  (-> (handler/api app-routes)
      (json/wrap-json-params)
      (json/wrap-json-response)))
