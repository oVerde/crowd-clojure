# SENDEMAIL v1

Endpoint: https://api-sendemail-dot-crowd-167215.appspot.com

Method: `POST`

Body:

```json
{
  "body" : string,
  "subject" : string,
  "from-email" : "contato@crowd.br.com",
  "to-email" : string,
}
```

Retorno:

*O mesmo do body*