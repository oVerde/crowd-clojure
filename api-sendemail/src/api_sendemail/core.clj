(ns api-sendemail.core
  (:require [compojure.core :refer :all]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [ring.middleware.json :as json]
            [ring.util.response :refer [created, response]]
            [postal.core :as p]))

(def conn {:host "smtp.gmail.com"
           :ssl  true
           :user "contato@crowd.br.com"
           :pass "WorkingCrowd72"})

(defn send-msg [msg]
  (p/send-message conn msg))

(defn create-msg [body subject from-email to-email]
  (hash-map :to to-email
            :from from-email
            :subject subject
            :body body))

;(send-msg (create-msg "teste" "Assunto" "contato@crowd.br.com" "danilo@crowd.br.com")) ;REPL

(defroutes app-routes
           (POST "/v1" {:keys [params]}
             (let [{:keys [body subject from-email to-email]} params]
               (println params)
               (send-msg (create-msg body subject from-email to-email))
               (response params)))
           (route/resources "/")
           (route/not-found "Not Found"))

;; MAIN
(def app
  (-> (handler/api app-routes)
      (json/wrap-json-params)
      (json/wrap-json-response)))