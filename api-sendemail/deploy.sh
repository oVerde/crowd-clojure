#!/usr/bin/env bash
lein ring uberwar
cd target
jar xvf api-brefing-0.1.0-SNAPSHOT-standalone.war
cd ..
cp appengine-web.xml target/WEB-INF
cd target
gcloud app deploy