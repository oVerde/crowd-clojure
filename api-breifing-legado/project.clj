(defproject api-brefing "0.1.0-SNAPSHOT"
  :description "Crowd API - Brefing"
  :url "www.crowd.com.br"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/java.jdbc "0.7.5"]
                 [compojure "1.5.1"]
                 [ring/ring-core "1.3.2"]
                 [ring/ring-json "0.3.1"]
                 [clj-http "3.9.0"]
                 [korma "0.3.0-RC5"]
                 [com.microsoft.sqlserver/mssql-jdbc "6.3.6.jre8-preview"]
                 [ring/ring-defaults "0.2.1"]
                 [clj-time "0.14.3"]]
  :plugins [[lein-ring "0.9.6" :exclusions [org.clojure/clojure]]]
  :ring {:handler api-brefing.core/app})
