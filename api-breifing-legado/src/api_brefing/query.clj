(ns api-brefing.query
    (:require [clojure.java.jdbc :as j]
              [clj-time.core :as t]
              [clj-time.coerce :as coerce]
              [clj-time.local :as l]))


(def db-spec {:dbtype   (if (= (System/getenv "ambiente") "prod") 
                                (System/getenv "dbtype_prd")
                                (System/getenv "dbtype_hm"))
              :dbname   (if (= (System/getenv "ambiente") "prod") 
                            (System/getenv "dbname_prd")
                            (System/getenv "dbname_hm"))
              :server   (if (= (System/getenv "ambiente") "prod") 
                            (System/getenv "server_prd")
                            (System/getenv "server_hm"))
              :user     (if (= (System/getenv "ambiente") "prod") 
                            (System/getenv "user_prd")
                            (System/getenv "user_hm"))
              :password (if (= (System/getenv "ambiente") "prod") 
                            (System/getenv "password_prd")
                            (System/getenv "password_hm"))})

(defn sql-now[]
    "Retorna a data atual em sql data"
    (coerce/to-sql-time (l/local-now)))

(defn insert-project [idCustomer nomeProjeto]
    (get (first (j/insert! db-spec :crowd_projects
                 {:active 1
                  :created_at (sql-now)
                  :id_customer idCustomer
                  :name nomeProjeto
                  :color "black"})) :generated_keys))

(defn insert-briefings [idCustomer nomeProjeto html idUsuario excerpt]
   { :idBreifing (get (first (j/insert! db-spec :crowd_briefings
         { :title nomeProjeto
            :text html
            :active 1
            :id_user idUsuario
            :sended 0
            :hunting 1
            :id_project (insert-project idCustomer nomeProjeto)
            :created_at (sql-now)
            :excerpt excerpt})) :generated_keys) })
             
        