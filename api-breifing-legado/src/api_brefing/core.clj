(ns api-brefing.core
  (:require [compojure.core :refer :all]
    [compojure.handler :as handler]
    [clj-http.client :as c]
    [compojure.route :as route]
    [ring.middleware.json :as json]
    [ring.util.response :refer [response, created]]
    [api-brefing.query :refer :all]))

;(def nomeProjeto "Teste")
;(def json-body (str "{\n\t\"body\": \"Chegou um novo briefing para avaliar\",\n\t\"subject\": \"[CROWD MATCH] "nomeProjeto"\",\n\t\"from-email\": \"danilo@crowd.br.com\",\n\t\"to-email\": \"danilo@crowd.br.com\"\n}"))

(defroutes app-routes
    (POST "/api/briefinglegado" {:keys [params]}
      (let [{:keys [idCustomer nomeProjeto html idUsuario excerpt]} params]
        (let [json-body (str "{\n\t\"body\": \""nomeProjeto excerpt html"\",\n\t\"subject\": \"[CROWD MATCH] "nomeProjeto"\",\n\t\"from-email\": \"danilo@crowd.br.com\",\n\t\"to-email\": \"plataforma@crowd.br.com\"\n}")]
          (c/post "https://api-sendemail-dot-crowd-167215.appspot.com/v1" {:body json-body :content-type :json})
          (response (insert-briefings idCustomer nomeProjeto html idUsuario excerpt)))))
    (route/resources "/")
    (route/not-found "Not Found"))

(def app
     (-> (handler/api app-routes)
       (json/wrap-json-params)
       (json/wrap-json-response)))