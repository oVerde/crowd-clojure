(ns api-matching.algolia)

(def app-id "Z4V86RWKZ2")
(def api-key "3f7e6ad21e56af31bcbe84205f1dda59")
(def client (.build (com.algolia.search.ApacheAPIClientBuilder. app-id api-key)))
(def index (.initIndex client "freelancers"))

(defn buscaFreelance [nome]
    (.search index (.setHitsPerPage (com.algolia.search.objects.Query. nome) 1000000)))