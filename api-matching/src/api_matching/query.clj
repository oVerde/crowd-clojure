(ns api-matching.query
    (:require [clojure.java.jdbc :as j]
              [clj-time.core :as t]
              [clj-time.coerce :as coerce]
              [clj-time.local :as l]
              [clj-http.client :as client]
              [clojure.data.json :as json]
              [api-matching.algolia :refer :all]))


    (def db-spec {:dbtype   (if (= (System/getenv "ambiente") "prod") 
                (System/getenv "dbtype_prd")
                (System/getenv "dbtype_hm"))
            :dbname   (if (= (System/getenv "ambiente") "prod") 
            (System/getenv "dbname_prd")
            (System/getenv "dbname_hm"))
            :server   (if (= (System/getenv "ambiente") "prod") 
            (System/getenv "server_prd")
            (System/getenv "server_hm"))
            :user     (if (= (System/getenv "ambiente") "prod") 
            (System/getenv "user_prd")
            (System/getenv "user_hm"))
            :password (if (= (System/getenv "ambiente") "prod") 
            (System/getenv "password_prd")
            (System/getenv "password_hm"))})

(defn sql-now[]
    "Retorna a data atual em sql data"
    (coerce/to-sql-time (l/local-now)))

(defn getText [id]
    (get (first 
        (j/query db-spec ["SELECT CONCAT(title, excerpt, text) as texto FROM crowd_briefings WHERE id = ?" id]))
        :texto))

(defn getFreelancerDoBriefing [id]
    (map :freelancer_id 
        (j/query db-spec ["SELECT freelancer_id FROM crowd_briefings_freelancers WHERE briefing_id = ?" id])
        ))

(defn procuraBreifing [id]
   (if (= (getText id) nil) "" 
    (clojure.string/replace  (getText id)  #"<(?:[^>=]|='[^']*'|=\"[^\"]*\"|=[^'\"][^\\s>]*)*>" "")))

(defn acessaNaturalLanguage [id]
    (json/read-str (get  (client/post "https://language.googleapis.com/v1/documents:analyzeEntities?key=AIzaSyCCL8JQUTVKZCNbB_XMr0bLoyABeerEAxw" 
        {:form-params {
            :document {
                    :type  "PLAIN_TEXT",
                    :language "pt-br",
                    :content (procuraBreifing id)
                  },
            :encodingType "UTF8",
          }
        :content-type :json}) :body) :key-fn keyword))

(defn trataTexto [id]
    (filter (fn [x] (not (= (get x :type) "PERSON")))
        (filter (fn [x] (not (= (get x :type) "ORGANIZATION")))
            (reverse (sort-by :salience (get (acessaNaturalLanguage id) :entities))))))

(defn retornaString [id]
    (clojure.string/join " " (take 5 (distinct 
        (map (fn [x] (clojure.string/upper-case (get x :name))) (trataTexto id))))))

(defn insertBriefingsFreelance [idbriefing idfreelance]
    (j/insert! db-spec :crowd_briefings_freelancers
        { :briefing_id idbriefing
          :freelancer_id idfreelance}))

(defn getArrayFreelancer [id result]
    (def listaFreelas (getFreelancerDoBriefing id))
    (take 10 (filter (fn [x] (not (.contains (java.util.ArrayList. listaFreelas) (Integer/parseInt x)))) 
        (map (fn [x] (.get x "Id")) (.getHits result)))))

 (defn acessaAlgolia [id]
    (def result (buscaFreelance (retornaString id)))
    (def resultado { :retorno (.getHits result) })
    (if (> (.getNbHits result) 50) (map (fn [idFrela] (insertBriefingsFreelance id idFrela)) 
        (getArrayFreelancer id result)) ""))


             