(ns api-matching.core
  (:require [compojure.core :refer :all]
    [compojure.handler :as handler]
    [compojure.route :as route]
    [ring.middleware.json :as json]
    [ring.util.response :refer [response, created]]
    [api-matching.query :refer :all]))

(java.security.Security/setProperty "networkaddress.cache.ttl" "60");

(defroutes app-routes
    (POST "/api/match" {:keys [params]}
      ;(let [{:keys [idBreifing]} params]
      ;  (response (acessaAlgolia idBreifing))
        (response ""))
    (route/resources "/")
    (route/not-found "Not Found"))

(def app
     (-> (handler/api app-routes)
       (json/wrap-json-params)
       (json/wrap-json-response)))