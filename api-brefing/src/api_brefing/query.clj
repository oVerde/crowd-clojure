(ns api-brefing.query
    (:require [api-brefing.datastore :as ds])
    (:import (com.google.appengine.api.datastore
        Query
        Query$FilterOperator)))


    (defn insert-breifing [idUsuarioFire html idCostumer idUsuario]
        (ds/create {:kind "BriefingHome" :idUsuarioFirebase idUsuarioFire :html (com.google.appengine.api.datastore.Text. html) 
                    :idCostumer idCostumer :idUsuario idUsuario}))

    (defn procura-usuario [id]     
        (ds/find-all (doto (Query. "BriefingHome") (.addFilter "idUsuarioFirebase" 
            Query$FilterOperator/EQUAL id))))
    
    (defn procurar-por-id [idUsuario]     
       (ds/find-all (doto (Query. "BriefingHome") (.addFilter "idUsuario" 
            Query$FilterOperator/EQUAL (Integer/parseInt idUsuario)))))