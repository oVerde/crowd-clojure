(ns api-brefing.core
  (:require [compojure.core :refer :all]
    [compojure.handler :as handler]
    [compojure.route :as route]
    [ring.middleware.json :as json]
    [ring.util.response :refer [created, response]]
    [api-brefing.query :refer :all]))

(defroutes app-routes
    (POST "/api/breifing" {:keys [params]}
      (let [{:keys [idUsuarioFire html idCostumer idUsuario]} params]
        (println params)
        (insert-breifing idUsuarioFire html idCostumer idUsuario)
        (created "")))
    (GET "/api/breifing/:id" [id] 
      (def resposta (procura-usuario id))
      (println resposta)          
      (response (map 
        (fn [resultado]  {:idUsuario (get resultado :idUsuario) :idCostumer (get resultado :idCostumer)})
        resposta)))
    (GET "/api/breifing/usuario/:idUsuario" [idUsuario]            
      (response (procurar-por-id idUsuario)))
    (route/resources "/")
    (route/not-found "Not Found"))
    

(def app
     (-> (handler/api app-routes)
       (json/wrap-json-params)
       (json/wrap-json-response)))