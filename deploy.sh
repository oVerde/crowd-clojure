#!/usr/bin/env bash
cd frontend
npm run build
rm -rf upload
mkdir upload
mkdir upload/build
cp app.yaml build
cp dispatch.yaml build
cp build upload/build
cd upload
gcloud app deploy
cd ../..
cd api-brefing
lein ring uberwar
cd target
jar xvf api-brefing-0.1.0-SNAPSHOT-standalone.war
cd ..
copy appengine-web.xml target/api-brefing-0.1.0-SNAPSHOT-standalone/WEB-INF
cd target/api-brefing-0.1.0-SNAPSHOT-standalone
appcfg update .
cd api-sendemail
lein ring uberwar
cd target
jar xvf api-brefing-0.1.0-SNAPSHOT-standalone.war
cd ..
cp appengine-web.xml target/WEB-INF
cd target
gcloud app deploy
cd ../../..
cd api-breifing-legado
lein ring uberwar
cd target
jar xvf api-brefing-0.1.0-SNAPSHOT-standalone.war
cd ..
copy appengine-web.xml target/api-brefing-0.1.0-SNAPSHOT-standalone/WEB-INF
cd target/api-brefing-0.1.0-SNAPSHOT-standalone
appcfg update .
cd ../../..
cd api-usuario-legado
lein ring uberwar
cd target
jar xvf api-brefing-0.1.0-SNAPSHOT-standalone.war
cd ..
copy appengine-web.xml target/api-brefing-0.1.0-SNAPSHOT-standalone/WEB-INF
cd target/api-brefing-0.1.0-SNAPSHOT-standalone
appcfg update .
