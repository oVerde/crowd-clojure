(ns token.query
  (:require [clojure.data.json :as json]
            ; [token.datastore :as ds]
            [clj-http.client :as client]
            [buddy.sign.jwt :as jwt]
            [clojure.data.json :as ja]
            [clojure.tools.logging :as log])
  (:import [java.security MessageDigest]
           (com.twilio.jwt.accesstoken AccessToken$Builder ChatGrant)
           (com.twilio Twilio)
           (com.twilio.rest.chat.v2.service Channel)))

(def account-sid "AC27412e037e1589853ff5118e835e090d")
(def api-key     "SK85672c46a6b7c769ca35634403e26e46")
(def api-secret  "31c3b82a7ed98936179fbca10874b5ad")
(def api-auth    "3nFlsX6pExcScf5wdqy323Rgm30GNI1N")
(def inicio      (Twilio/init  account-sid api-secret))

(def service-sid "IS36d2fce72141405eb91fd1604347f07e")

(def grant (-> (ChatGrant.)
               (.setServiceSid service-sid)))

(defn token-twilio [uid] (.toJwt (-> (AccessToken$Builder. account-sid api-key api-secret)
                                     (.identity uid) ;DEVE SER RECEBIDO UM IDENTIFICADOR UNICO DO USUÁRIO
                                     (.grant grant)
                                     (.build))))

(defn validaToken [token]
  (try (jwt/unsign token "crowd")
       (catch Exception e "Token invalido")))

(defn gerarChannel [nome token]
  (def tokenValidado (validaToken token))
  (if (= tokenValidado "Token invalido") "token.invalido"
      (.getSid (-> (Channel/creator service-sid)
                   (.setFriendlyName nome)
                   (.create)))))

(defn gera-toke-twilio [token]
  (def tokenValidado (validaToken token))
  (if (= tokenValidado "Token invalido") "token.invalido"
      (token-twilio (get tokenValidado :id))))

(defn send-sms-verify [phone]
  (def call (client/post "https://api.authy.com/protected/json/phones/verification/start"
                         {:form-params {:via "sms" :phone_number phone
                                        :country_code 55 :locale "pt-BR"
                                        :code_length 6} ;; precisa ser 6 dígitos para acompanhar o frontend/app
                          :headers {"X-Authy-API-Key" api-auth}
                          :content-type :json}))
  (def result (ja/read-str (get call :body) :key-fn keyword))
  (if (= (get result :success) true)
    {:message (get result :message)
     :seconds_to_expire (get result :seconds_to_expire)
     :uuid (get result :uuid)}
    "sms.not.sended"))

(defn verify-sms-token
  [phone code uuid]
  "Checa o SMS com a Twilio e retorna se ele está correto ou não"
  (try (let [call (client/get
                    (format "https://api.authy.com/protected/json/phones/verification/check?phone_number=%s&country_code=55&verification_code=%s" phone code)
                    {:headers {"X-Authy-API-Key" api-auth}
                     :accept :json})]
            (let [result (ja/read-str (get call :body) :key-fn keyword)]
              (if (= (result :success) true)
                  true)))

       ;; Twilio retorna erro HTTP se o código não for validado corretamente
       (catch Exception e
         (log/info "Erro na validação do SMS " e) ; Log do catch se estiver errado ou já validado
         (let [call (client/get
                      (format "https://api.authy.com/protected/json/phones/verification/status?uuid=%s&country_code=55&phone_number=%s" uuid phone)
                      {:headers {"X-Authy-API-Key" api-auth}
                       :accept :json})]
              (let [result (ja/read-str (get call :body) :key-fn keyword)]
                (log/info "Twilio: " result) ; Motivo de ter sido verificado duas vezes
                (ja/write-str result))))))

;; NOTE: snippets de código para rápida checagem
; (send-sms-verify "+5511994805660")
; (verify-sms-token "+5511994805660" "621625" "b59d2200-9e6e-0136-a177-0a5b7c2a32fe")
