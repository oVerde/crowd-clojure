(ns token.core
  (:require [compojure.core :refer :all]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [clojure.data.json :as j]
            [ring.middleware.json :as json]
            [ring.util.response :refer [response, created]]
            [token.query :refer :all]
            [clojure.tools.logging :as log]))

(defroutes app-routes
           (POST "/send-sms-verify" {:keys [params]}
                 (let [{:keys [phone]} params]
                   (log/info "Enviando SMS de verificação para: " phone)
                   (response (send-sms-verify phone))))
           (POST "/verify-sms-token" {:keys [params]}
                 (let [{:keys [phone code uuid]} params]
                   (log/info "Verificando código SMS com: " params)
                   (response (verify-sms-token phone code))))
           ; (GET "/api/usuariolegado" [email]
           ;      (response (procura-login-existente email)))
           (route/resources "/")
           (route/not-found "Not Found"))

(def app
  (-> (handler/api app-routes)
      (json/wrap-json-params)
      (json/wrap-json-response)))
