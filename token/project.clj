(defproject api-brefing "0.1.0-SNAPSHOT"
            :description "Crowd API - Token"
            :url "www.crowd.com.br"
            :license {:name "Eclipse Public License"
                      :url "http://www.eclipse.org/legal/epl-v10.html"}
            :dependencies [[org.clojure/clojure "1.9.0"]
                           [org.clojure/java.jdbc "0.7.5"]
                           [ring/ring-core "1.3.2"]
                           [ring/ring-json "0.3.1"]
                           [ring/ring-defaults "0.2.1"]
                           [compojure "1.6.1"]
                           [clj-http "3.9.1"]
                           ; [korma "0.3.0-RC5"]
                           [buddy "2.0.0"]
                           [clj-time "0.14.3"]
                           [org.clojure/data.json "0.2.6"]
                           ; [com.walmartlabs/lacinia "0.21.0"]
                           [io.aviso/logging "0.2.0"]
                           [org.clojure/tools.logging "0.3.1"]
                           [log4j/log4j "1.2.17" :exclusions [javax.mail/mail
                                                              javax.jms/jms
                                                              com.sun.jmdk/jmxtools
                                                              com.sun.jmx/jmxri]]
                           [com.google.cloud/google-cloud-logging-logback "0.45.0-alpha"]
                           [com.fasterxml.jackson.core/jackson-databind "2.9.5"]
                           [com.twilio.sdk/twilio "7.17.0"]
                           [com.fasterxml.jackson.core/jackson-annotations "2.9.5"]
                           [com.fasterxml.jackson.core/jackson-core "2.9.5"]
                           [proto-repl "0.3.1"]]
            :plugins [[lein-ring "0.9.6" :exclusions [org.clojure/clojure]]]
            :ring {:handler token.core/app}
            :main token.core)
